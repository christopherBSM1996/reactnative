import React, {Component, Fragment}from "react";
import {TouchableOpacity,Text,StyleSheet} from 'react-native'


class ActionButton extends Component{
    render(){
        const {reset, plus}= this.props
        return (
            <Fragment>
                <TouchableOpacity style={styles.btnReset} onPress={reset}>
                    <Text style={styles.btnTxt}>Reset</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.btnReset} onPress={plus}>
                <Text style={styles.btnTxt}>+10</Text>
                </TouchableOpacity>
            </Fragment>
        )
    }
}
const styles = StyleSheet.create({
    btnTxt:{
        fontSize:30,
        color:'#2d3436',
        fontWeight:'bold',//marca mas el color 
    },
    btnReset:{
        height:50,
        width:120,
        backgroundColor:'#95a5a6',
        justifyContent:'center',
        alignItems:'center',
        marginHorizontal:7,
    },
})

export default ActionButton