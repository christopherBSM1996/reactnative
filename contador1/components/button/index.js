import React from "react"
import {Text,TouchableOpacity,StyleSheet} from "react-native"
import PropTypes from 'prop-types'

function Button (props){
    const {label, action}=props
    console.log('hola')
    
    return (
        <TouchableOpacity style={styles.btn} onPress={action}>{/*hace la funcion de un botón */}
          <Text style={styles.btnTxt}>{label}</Text>
        </TouchableOpacity>
    )
}

Button.propTypes ={
  label: PropTypes.string.isRequired,//nos sirve para exigir la variable 
  action: PropTypes.func,
}

const styles = StyleSheet.create({
    btn:{
      height:50,
      width:50,
      justifyContent:'center',
      alignItems:'center',
      backgroundColor:'#95a5a6'
    },
    btnTxt:{
      fontSize:30,
      color:'#2d3436',
      fontWeight:'bold',//marca mas el color 
    },
  });
  

export default Button