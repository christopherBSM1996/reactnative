/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState}from 'react';
import type {Node} from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text
} from 'react-native';
import Button from './components/button';
import ActionButton from './components/actionButton';



const App: () => Node = () => {
  const [counter, setcounter] = useState(0)
  function handledown(){
    setcounter(counter-1)
  }
  function handleUp(){
    setcounter(counter+1)
  }
  function handleReset(){
    setcounter(0)
  }
  function handlePlusten(){
    setcounter(counter+10)
  }
  return (  
    <View style={styles.container}>
      <View style={styles.subcontainer}>
        <Button label='-' action={handledown}/>
        <View style={styles.couterContainer}>
          <Text style={styles.couter}>{counter}</Text>
        </View>
        <Button label='+' action={handleUp}/>
      </View>
      <View style={styles.subcontainerReset}>
        <ActionButton reset={handleReset} plus={handlePlusten}></ActionButton>
      </View>
    </View>
  )
};

const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'#2c3e50',
    justifyContent:'center'
  },
  subcontainer:{
    height:50,
    width:'100%',
    paddingHorizontal:10,
    flexDirection:'row',
  },
  subcontainerReset:{
    height:50,
    width:'100%',
    paddingHorizontal:10,
    flexDirection:'row',
    justifyContent:'center',
    alignItems:'center',
    marginTop:20,
  },
  btn:{
    height:50,
    width:50,
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:'#95a5a6'
  },
  btnTxt:{
    fontSize:30,
    color:'#2d3436',
    fontWeight:'bold',//marca mas el color 
  },
  couterContainer:{
    flex:1,
    justifyContent:'center',
    alignItems:'center',
  },
  couter:{
    fontSize:40,
    color:'white',
    fontWeight:'bold'
  },
  
});

export default App;
