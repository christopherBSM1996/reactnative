/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type {Node} from 'react';
import Viewer from './components/view';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';




const App: () => Node = () => {
  
  return (
    // <View style={{flex:1, backgroundColor:'blue', justifyContent:'center', alignItems:'center'}}>
    <View style={styles.container}>
      <Viewer/>
      <View style={styles.containerRed}/>
      <View style={styles.containerGreen}/>
      <View style={{flex:.5}}/>
    </View>  
  );
};

const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'blue',
  },
  containerRed:{
    flex:1.2, 
    backgroundColor:'red',
  },
  containerGreen:{
    flex:.5, 
    backgroundColor:'green'
  }
});

export default App;
